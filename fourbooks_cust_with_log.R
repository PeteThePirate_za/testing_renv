
library(log4r)

const_prediction_date<-Sys.Date()
log_file <- paste0("Import_run_log_", gsub("-","",const_prediction_date),".log")
file_logger <- logger("INFO", appenders = file_appender(log_file))
sink(file = log_file, append = T, type = c("output", "message"),
     split = FALSE)
info(file_logger, "Beginning Run")




tryCatch({
  
  #import fourbooks_customers table
    library(RODBC)
    con<-odbcConnect(dsn="RODBCdsn")
  fourbooks_customers<-sqlQuery(con,
                                stringsAsFactors= F,
                                na.strings= c("","NULL","NA","Unknown","UNKNOWN","-99"),
                                query = "
                  select *
                    from customer_data_model.cyest.fourbookscustomers
                    "
  )
  #save copy into latest for auto-update
  saveRDS(fourbooks_customers, "./Imported Data/latest/fourbooks_customers_latest.rds")
  #save vintage of the copy for later reference
  saveRDS(fourbooks_customers,
          paste0("./Imported Data/vintage/fourbooks_customers_",gsub("-","_", Sys.Date()),".rds"))

}, error = function(err.msg){
  # Add error message to the error log file
  warn(file_logger, err.msg)
  sink(file = NULL)
  
  
}
)

sink(file = NULL)
rm(con, fourbooks_customers)
gc()
